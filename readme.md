# MarxManager app built with Laravel

MarxManager is a bookmark manager with registration and authentication of users. It allows to create and delete bookmarks by registered users.

##### Used technologies:

- Laravel - PHP Framework
- Bootstrap - CSS Framework
- [axios](https://www.npmjs.com/package/axios) - Promise based HTTP client for the browser and node.js.